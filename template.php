<?php

/**
* Return a themed breadcrumb trail.
*
* @param $breadcrumb
*   An array containing the breadcrumb links.
* @return
*   A string containing the breadcrumb output.
*/
function opengrid_academica_breadcrumb($breadcrumb) {
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('show_breadcrumbs');
  if($show_breadcrumb == 'yes'){
    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('breadcrumb_separator');
      $trailing_separator = '';
      if (theme_get_setting('breadcrumb_title')) {
	  $title = theme_get_setting('breadcrumb_title') . $breadcrumb_separator;
          $trailing_separator = $breadcrumb_separator;
      }
      return '<div class="breadcrumb">' . $title . implode($breadcrumb_separator, $breadcrumb) . "</div>";
    }    
  }
  // Otherwise, return an empty string.
  return '';  
}
/**
* Implementation of hook_theme().
*/
function opengrid_academica_theme(){
  return array(
    'comment_form' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

function opengrid_academica_preprocess_page($vars){
  //Slideshow 
  if(drupal_is_front_page() && theme_get_setting('slideshow_enabled')){
    $slideshow_photos = theme_get_setting('photo_names');
    $width = theme_get_setting('photo_names') ? theme_get_setting('slideshow_width') : '950';
    $height = theme_get_setting('photo_names') ? theme_get_setting('slideshow_height') : '355';    
    $slideshow_photos_array = explode(',', $slideshow_photos);
    foreach($slideshow_photos_array as $key => $name){
      $vars['slideshows'] .= '<img src="/'. path_to_theme() .'/images/slideshows/'. $name .'" width="'. $width .'" height="'. $height .'" alt="'. $vars['site_name'] .'"/>';
    }
  }
  //Social
  if(theme_get_setting('social_box') || module_exists('aggregator')){
    $social_links = theme_get_setting('social_links');
    foreach($social_links as $key => $value){
      if($value != '' && theme_get_setting('social_box')){
	$key_class = str_replace(' ','-', $key);
	$img_name = 'icon_'. $key .'.png';
	$img = '<img title ="'. $key .'" alt="'. $key .'" src="/'. path_to_theme() .'/images/social/'. $img_name .'" />';
	$social[$key] = l($img, $value, array('attributes' => array('class' => 'social-icon '. $key_class), 'html' => true));
      }  
    }
    if(module_exists('aggregator') ){
      $social['feed'] = $vars['feed_icons'];
    }
    $vars['social'] = theme('item_list', $social);
  }
  
}

function opengrid_academica_preprocess_node($vars){
 $links = $vars['node']->links;
  //looks inside the links array for the comment counter
  //and removes it
  foreach($links as $module => $data){
    if($module == 'comment_comments'){
      unset($links[$module]); 
    } 
  }
  //se vuelve  a armar la variable $links
  $vars['links'] = theme('links', $links);
}
/**
* Theme the output of the submitted variable.
*
* @param $node
*   The $node object.
*/
function opengrid_academica_node_submitted($node){
  $comments = '';
  if($node->teaser){
    if($node->comment_count ){
	$title = format_plural($node->comment_count, t('1 comment'), t('@count comments'));
	$comment_link = l($title, 'node/'. $node->nid, array('fragment' => 'comments'));
	$comments = '<span class="meta-info meta-comments">'.  $comment_link .'</span>';
    } else {
	$comments = t('No Comments');  
    }
    $date = '<spam class="time">'. format_date($node->created,'custom', 'F d, Y G A').'</spam>';
    $output = $date .' / '. $comments;
  } else {
    $output = '';  
  }
  return $output; 
}

/**
* Theme the output of the comment_form.
*
* @param $form
*   The form that  is to be themed.
*/
function opengrid_academica_comment_form($form) {
  
  $form['_author']['#prefix'] = '<div class="author-name">'; 
  $form['_author']['#suffix'] = '</div>';    
  $form['comment_filter']['format']['#type'] = 'hidden';
  $form['comment_filter']['comment']['#title'] = '';
  $form['comment_filter']['comment']['#rows'] = 7;    
 
  return drupal_render($form);
}


function opengrid_academica_preprocess_comment_wrapper($vars){
  $node = node_load(arg(1));
  $vars['comment_count'] = $node->comment_count != 0 ? format_plural($node->comment_count, t('1 comment'), t('@count comments')) : t('No comments');
  $vars['no_comments_message'] = $node->comment_count == 0 ? t('Be the first to post a comment') : '';
}
