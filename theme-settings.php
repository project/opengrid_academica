<?php

// Include the definition of opengrid_settings, opengrid_initialize_theme_settings() and opengrid_default_theme_settings().
include_once './' . drupal_get_path('theme', 'opengrid') . '/theme-settings.php';


/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function opengrid_academica_settings($saved_settings) {

  global $base_url;
  
  //print dsm($saved_settings);

  // Get theme name from url (admin/.../theme_name)
  $theme_name = arg(count(arg()) - 1);

  // Combine default theme settings from .info file & theme-settings.php
  $theme_data = list_themes();   // get data for all themes
  $info_theme_settings = ($theme_name) ? $theme_data[$theme_name]->info['settings'] : array();
  $defaults = array_merge(opengrid_default_theme_settings(), $info_theme_settings);
  
  // Combine default and saved theme settings
  $settings = array_merge($defaults, $saved_settings);
  
  // Create the form using Forms API: http://api.drupal.org/api/6
  $form = array();
  
  // Add the base theme's settings.
  $form += opengrid_settings($saved_settings, $defaults);
  $form['opengid_sa_message'] = array(
    '#type' => 'markup',
    '#value' => '<h3>'. t('Academic Settings.') .'</h3>',
  );  
  $form['breadcrumb'] = array(
      '#type' => 'fieldset',
      '#title' => t('Breadcrumb settings'),
      '#collapsible' => true,
      'collapsed' => false,
  );
  $form['breadcrumb']['show_breadcrumbs'] = array(
      '#type' => 'select',
      '#title' => t('Display breadcrumbs'),
      '#options' => array(
	      'yes' => t('Yes'),
	      'no' => t('No'),
      ),
      '#default_value' => $settings['show_breadcrumbs'],
  );
  $form['breadcrumb']['breadcrumb_separator'] = array(
      '#type' => 'textfield',
      '#title' => t('Prefix text for breadcrumbs'),
      '#size' => 4,
      '#default_value' => $settings['breadcrumb_separator'],
  );
  $form['breadcrumb']['breadcrumb_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title for breadcrumbs'),
      '#size' => 60,
      '#description' => t('Add an introduction text for breadrumbs like: You are here: '),
      '#default_value' => $settings['breadcrumb_title'],
  );
  
  $form['slideshow'] = array(
      '#type' => 'fieldset',
      '#title' => t('Slideshow Photos'),
      '#collapsible' => true,
      '#collapsed' => false,
      '#description' => t('Slideshow photos must be saved in slideswhos folder, this folder is placed inside the images folder of the theme.'),
  );
  $form['slideshow']['slideshow_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Slideshow enabled'),
      '#default_value' => $settings['slideshow_enabled'],
  );
  $form['slideshow']['photo_names'] = array(
      '#type' => 'textfield',
      '#title' => t('slideshow Photos names.'),
      '#description' => t('Separate each file name with comma.'),
      '#default_value' => $settings['photo_names'],
  ); 
  $form['slideshow']['slideshow_width'] = array(
      '#type' => 'textfield',
      '#title' => t('slideshow width.'),
      '#description' => t('Set the width for the slideshow box in pixels.'),
      '#default_value' => $settings['slideshow_width'],
  );
  $form['slideshow']['slideshow_height'] = array(
      '#type' => 'textfield',
      '#title' => t('slideshow height.'),
      '#description' => t('Set the height for the slideshow box in pixels.'),
      '#default_value' => $settings['slideshow_height'],
  );  
  
  $form['social'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Links'),
    '#collapsible' => true,
    '#collpased' => false,
  );
  $form['social']['social_box'] = array(
    '#type' => 'checkbox',
    '#title' => t('Social Box'),
    '#description' => t('Enables Social Box in theme header'),
    '#default_value' => $settings['social_box'],    
  );
  $form['social']['social_links'] = array(
    '#type' => 'fieldset',
    '#tree' => true,
  );
  $form['social']['social_links']['youtube'] = array(
    '#type' => 'textfield',
    '#title' => t('youtube'),
    '#default_value' => $settings['social_links']['youtube'],
    '#size' => 60,
    '#attributes' => array('class' => 'social youtube'),
  );
  $form['social']['social_links']['linkedin'] = array(
    '#type' => 'textfield',
    '#title' => t('linkedin'), 
    '#default_value' => $settings['social_links']['linkedin'],
    '#size' => 60,
    '#attributes' => array('class' => 'social linkedin'),
  );  
  $form['social']['social_links']['facebook'] = array(
    '#type' => 'textfield',
    '#title' => t('facebook'), 
    '#default_value' => $settings['social_links']['facebook'],
    '#size' => 60,
    '#attributes' => array('class' => 'social facebook'),
  ); 
  $form['social']['social_links']['flickr'] = array(
    '#type' => 'textfield',
    '#title' => t('flickr'), 
    '#default_value' => $settings['social_links']['flickr'],
    '#size' => 60,
    '#attributes' => array('class' => 'social flickr'),
  ); 
  $form['social']['social_links']['twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('twitter'), 
    '#default_value' => $settings['social_links']['twitter'],
    '#size' => 60,
    '#attributes' => array('class' => 'social twitter'),
  );        
  return $form;
}
