/*$Id: */

-- SUMMARY --

This theme is designed specifically for educational websites such as universities, schools etc. 
It’s a flexible and versatile free theme that can be easily customized and branded for any university, academy or non-profit organization.



-- REQUIREMENTS --

Opengrid Core:
  http://drupal.org/project/openg
  


-- INSTALLATION --

* Download both Opengrid Academica and Opengrid

* Unpack them as usual within your themes folder.

* Enable Opengrid academic on your themes page on example.com/admin/build/themes

* Set Opengrid Academic as your default theme



-- CREDITS --

This theme has been made possible by:

* MOJAH MEDIA
  Theming and support by:
  http://www.mojahmedia.com

* SMASHING MAGAZINE
  Distribution of original Wordpress Theme:
  http://www.smashingmagazine.com

* PROUDTHEMES DESIGN AGENCY
  Graphic design and original Wordpress theme by:
  http://www.proudthemes.com/
