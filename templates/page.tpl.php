<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
    <!--[if IE 7]>
      <?php print $ie7_styles; ?>
    <![endif]-->
    <!--[if lte IE 6]>
      <?php print $ie6_styles; ?>
    <![endif]-->  
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>

<body class="<?php print $body_classes; ?>">

  <div id="page" class="<?php print $page_classes; ?>">
  <div class="inner-page">
  
    <div class="header-wrapper"><div id="container-header" class="<?php print $header_classes; ?>">
	<div id="header" class="<?php print $header_grid; ?>">
	    <?php if(!empty($breadcrumb)): ?>
	      <div id="breadcrumbs">
		  <?php print $breadcrumb; ?>
	      </div>
	    <?php endif; ?>
	    	
	    <?php if(!empty($header)): ?>
		<div class="header-region">
		    <?php print $header; ?>
		</div>
	    <?php endif; ?>
	    
	    <?php if(!empty($logo)): ?>
		<a class="logo-img" title="<?php print $site_name; ?>" href="<?php print $base_path; ?>"><img alt="<?php print $site_name; ?>" src="<?php print $logo; ?>" /></a>
	    <?php endif; ?>
	    
	    <?php if(!empty($site_name) || !empty($site_slogan)): ?>
		<h1 class="site-name"><a href="<?php print $base_path;?>"><?php print $site_name; ?></a></h1>
		<div class="site-slogan"><?php print $site_slogan;?></div>
	    <?php endif;?>
	    
	    <?php if(!empty($social)): ?>
	      <div id="social"><?php print $social; ?></div>
	    <?php endif; ?>
	    
	</div>
    </div></div>
    
    <?php if(!empty($slideshows)): ?>
      <div id="slideshow-wrapper">
	<div class="slideshow">
	  <?php print $slideshows; ?>
	</div>
      </div>
    <?php endif; ?> 
    
    <div class="content-top-wrapper"><div id="container-content-top" class="<?php print $content_top_classes; ?>">
	<div id="content-top" class="<?php print $content_top_grid; ?>">
	    <?php print $content_top; ?>
	     <?php if (!empty($tabs)): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
             <?php if (!empty($title)): ?><h1 class="title <?php print $title_class; ?>" id="page-title" ><?php print $title; ?></h1><?php endif; ?>
             <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div></div><?php endif; ?>
	</div>     
    </div></div>
    
    <div class="content-wrapper"><div id="container-content" class="<?php print $content_classes; ?>">
	<div id="content" class="<?php print $content_grid; ?>">

	  <div id="main" class="<?php print $main_grid; ?>">
	     <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
             <?php if (!empty($messages)): print $messages; endif; ?>
             <?php if (!empty($help)): print $help; endif; ?>	
             <?php if(!empty($mission)): ?>
                <div id="mission">
		    <?php print $mission; ?>
                </div>
             <?php endif; ?><!-- #mission -->  

	    <div class="content-output">
		<?php print $content; ?>
	    </div>
	  </div><!-- #main ends here --> 
	  
	  <?php if(!empty($first_sidebar)): ?>
	      <div id="first-sidebar" class="<?php print $first_sidebar_grid; ?>">
		  <?php print $first_sidebar; ?>
	      </div>
	  <?php endif; ?><!-- #first ends here -->

	  <?php if(!empty($second_sidebar)): ?>
	      <div id="second-sidebar" class="<?php print $second_sidebar_grid; ?>">
		  <?php print $second_sidebar; ?>
	      </div>
	  <?php endif; ?> <!-- #second ends here -->

	</div>    
    </div></div><!-- #content ends here --> 
    
    <div class="content-bottom-wrapper"><div id="container-content-bottom" class="<?php print $content_bottom_classes; ?>">
	<div id="content-bottom" class="<?php print $content_bottom_grid; ?>">
	    <?php print $content_bottom; ?>
	</div>    
    </div></div> 
    
    <?php if($primary_links): ?>
      <div id="main-navigation-wrapper">
	<div id="main-navigation" class="<?php print $main_navigation_classes?>">
	  <div class="primary-links"><?php print $primary_links; ?></div>
	  <div class="secondary-links"><?php print $secondary_links; ?></div>
	</div>
      </div>
    <?php endif;?> 
    
    <?php if($scroll_to_top): ?>
      <div class="scroll-to-top"><a href="#header"><?php print $scroll_to_top; ?></a></div>
    <?php endif; ?>
    
  
    <div class="footer-wrapper"><div id="container-footer" class="<?php print $footer_classes; ?>">
	<div id="footer" class="<?php print $footer_grid; ?>">
	    <?php if(!empty($footer)): ?>
		<div id="footer-region"><?php print $footer; ?></div>
	    <?php endif; ?>
	    
	    <?php if(!empty($footer_message)): ?>
		<div class="footer-message"><?php print $footer_message; ?></div>
	    <?php endif; ?>
	    
	</div>    
    </div></div>
  </div><!-- .inner-page ends here --> 
    <?php if(!empty($tray_region)): ?>
	<div class="tray-close">Tray</div>    
	<div id="tray-wrapper">
	  <?php print $tray_region; ?>
	</div>
    <?php endif; ?>    
    <?php print $closure; ?>

  </div><!-- #page ends here -->
  <?php print $grid_ui; ?>
</body>
</html>
