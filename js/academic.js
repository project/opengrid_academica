
Drupal.behaviors.academicjs = function(context) {
    $('.slideshow').cycle({
		fx: 'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
		speed: 5000,
		timeout: 5000,
		cleartype:  1 
    });
    $('.superfish ul li').mouseover(function(){
      $(this).addClass('hover');  
    }).mouseout(function(){
      $(this).removeClass('hover');  
    });
}
